package main

import (
	"fmt"
	"math/rand"
	"os"
	"runtime"
	"sort"
	"strconv"
	"text/tabwriter"
	"time"

	kp "github.com/alecthomas/kingpin"
)

const maxNumbers int = 24

var (
	app = kp.New("ktem-generator", "Kaikki tai ei mitään -pelin numerogeneraattori.")

	analyze      = app.Command("analyze", "Testaa KTEM numeroiden jakauma pitkällä aikavälillä simulaation avulla.")
	goRoutineNum = analyze.Flag("routines", "Go-rutiineiden määrä.").Required().Int()
	simulations  = analyze.Flag("sims", "Kuinka monta kertaa simuloidaan.").Required().Int()

	generateCmd = app.Command("generate", "Arpoo yhden rivin.")
	rowsNum     = generateCmd.Arg("rows", "Rivien määrä.").Int()

	ownNumbersCmd = app.Command("own", "Anna omia lukuja.")
	ownRows       = ownNumbersCmd.Flag("rows", "Omien lukujen määrä").Short('r').Int()
	ownNumbers    = ownNumbersCmd.Arg("numbers", "Anna omat luvut, maks. 24 numeroa.").Required().Ints()
	currOS        = runtime.GOOS
)

func main() {

	// Seed the random number generator
	rand.Seed(time.Now().UnixNano())

	// Parse kingpin commands
	switch kp.MustParse(app.Parse(os.Args[1:])) {
	case generateCmd.FullCommand():

		if *rowsNum == 0 {
			fmt.Printf("Arvotut numerot: %v\n", generate(12))
		} else {
			for i := 0; i < *rowsNum; i++ {
				fmt.Printf("%d. arvottu rivi: %v\n", i+1, generate(12))
			}
		}

	case ownNumbersCmd.FullCommand():

		if len(*ownNumbers) >= 12 {
			fmt.Println("Annoit 12 omaa lukua, tai enemmän. Kysypä joku toinen.")
			return
		}

		if *ownRows == 0 {
			fmt.Printf("Omien lukujen pohjalta arvottu rivi: %v\n", combine(*ownNumbers, generate))
		} else {
			for i := 0; i < *ownRows; i++ {
				fmt.Printf("%d. yhdistelmärivi: %v\n", i+1, combine(*ownNumbers, generate))
			}
		}

	case analyze.FullCommand():
		simulate(*goRoutineNum, *simulations)

	}
}

// Generates a single row of numbers.
func generate(l int) []int {

	n := []int{}
	n = rand.Perm(maxNumbers + 1) // Randomize numbers from 0 to 24
	for i, v := range n {
		if v == 0 {
			// Delete the zero value
			n = append(n[:i], n[i+1:]...)
		}
	}

	n = n[:l]
	sort.Ints(n)

	return n
}

// Combine user-defined and generated row.
func combine(own []int, gen func(int) []int) []int {
	genRow := gen(12)
	genRow = genRow[len(own):len(genRow)]
	own = append(own, genRow...)

	sort.Ints(own)

	// Check for duplicates, and they are found, generate new ones until no duplicates are found.
	for i := 0; i < len(own)-2; i++ {
		if own[i] == own[i+1] {
			fmt.Printf("Kaksi samaa arvoa löydetty! Arvo: %d\n", own[i])
			own = append(own[:i], own[i+1:]...)

			for {
				g := gen(1)[0]
				fmt.Printf("Generoitiin uusi luku: %d\n", g)
				found := false
				for j := 0; j < len(own)-2; j++ {
					if own[j] == g {
						fmt.Printf("Validointikierros, löytyi uusi duplikaatti, arvo: %d\n", own[j])
						found = true
					}
				}
				if !found {
					fmt.Println("Validointikierros valmis ja uusia duplikaatteja ei löytynyt.")
					own = append(own, g)
					sort.Ints(own)
					break
				}
			}
		}
	}

	return own
}

// Run a KTEM simulation to see, how are the numbers going to be distributed.
func simulate(workers, simCount int) {

	var resultSlc []int
	simulations := make(chan int, simCount)
	rows := make(chan []int)

	simWorker := func(id int, simulations <-chan int, rows chan<- []int) {
		for s := range simulations {
			// fmt.Printf("worker %d, simulation %d\n", id+1, s)
			s++
			rows <- generate(12)
		}
	}

	// Start N workers
	t1 := time.Now()
	fmt.Printf("Aloitettiin simultointi %d workerilla.\n", workers)
	for i := 0; i < workers; i++ {
		go simWorker(i, simulations, rows)
	}

	// Execute N simulations on each worker
	for j := 0; j < simCount; j++ {
		simulations <- j
	}
	close(simulations)

	// Get results
	for r := 0; r < simCount; r++ {
		res := <-rows
		resultSlc = append(resultSlc, res...)
	}

	fmt.Printf("Simulointi päättyi onnistuneesti, ja se kesti %v.\n", time.Now().Sub(t1))

	// Sort the slice and start counting results
	resultMap := make(map[int]int, 0)
	sort.Ints(resultSlc)

	c := 1
	for i, v := range resultSlc {

		// Reset count back to one if current value is different from the previous one
		if i > 0 && v != resultSlc[i-1] {
			c = 1
		}

		resultMap[v] = c
		c++
	}

	f := func(m map[int]int) (s string) {
		for _, v := range m {
			s += strconv.Itoa(v) + "\t"
		}
		s += " kpl."
		return
	}

	w := new(tabwriter.Writer)
	w.Init(os.Stdout, 6, 0, 1, ' ', 0)
	fmt.Fprintf(w, "Simulaation tulokset\n=============================================\n")
	fmt.Fprintln(w, "1\t2\t3\t4\t5\t6\t7\t8\t9\t10\t11\t12\t13\t14\t15\t16\t17\t18\t19\t20\t21\t22\t23\t24\t")
	fmt.Fprintln(w, f(resultMap))
	w.Flush()
}
